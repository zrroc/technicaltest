//
//  Dynamic.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

class Dynamic<T> {
   typealias Listener = T->Void
    var listener: Listener?
    
    func bind(listener:Listener?) {
        self.listener = listener
    }
    
    func bindAndFire(listener:Listener?) {
        self.listener = listener
        listener?(value!)
    }
    
    var value: T?{
        didSet {
            listener?(value!)
        }
    }
    
    init(_ v:T) {
        value = v
    }
    
    init() {
        
    }
}
