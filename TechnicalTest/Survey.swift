//
//  Survey.swift
//  TechnicalTest
//
//  Created by zrroc on 16/8/8.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Survey: Object, Mappable {
    dynamic var surveyTitle = ""
    dynamic var surveyDescription = ""
    dynamic var surveyCoverImageUrl = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        surveyTitle <- map["title"]
        surveyDescription <- map["description"]
        surveyCoverImageUrl <- map["cover_image_url"]
    }
}
