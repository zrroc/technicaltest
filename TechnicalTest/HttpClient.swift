//
//  HttpClient.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import Foundation
import Alamofire

class HttpClient {
    
    static let sharedInstance: HttpClient = {
        return HttpClient()
    }()
    
    func request(requestRouter:URLRequestConvertible,completionBlock:(data:AnyObject?,error:ErrorType?)->Void) {
        Alamofire.request(requestRouter).responseJSON { (_, _, response) in
            if response.isSuccess {
                completionBlock(data: response.value!,error: nil)
            }else{
                completionBlock(data: nil, error: response.error!)
            }
        }
    }
}