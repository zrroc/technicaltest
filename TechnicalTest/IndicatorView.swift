//
//  IndicatorView.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import UIKit

public let kIndicatorWidth: CGFloat = 20
public let kIndicatorSpacing: CGFloat = 2
public let kIndicatorImageTag = 100

class IndicatorView: UIView {
    
    var selectedIndex: Int = 0
    /**
     init view by circle count
     
     - parameter numberOfIndicator: count of the circle in view
     
     - returns: self
     */
    init(numberOfIndicator: Int) {
        super.init(frame: CGRectZero)
        var indicatorImage: UIImageView!
        for i in 0..<numberOfIndicator {
            indicatorImage = UIImageView(frame: CGRectMake(0, (kIndicatorWidth + kIndicatorSpacing) * CGFloat(i), kIndicatorWidth, kIndicatorWidth))
            indicatorImage.tag = kIndicatorImageTag + i
            if i == selectedIndex {
                indicatorImage.image = UIImage(named: "circle_fill")
            }else{
                indicatorImage.image = UIImage(named: "circle_stroke")
            }
            addSubview(indicatorImage)
        }
        frame.size = CGSizeMake(kIndicatorWidth, indicatorImage.frame.maxY)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     Update selected circle
     
     - parameter index: the new selected index
     */
    func updateSelectedIndicatorIndex(index:Int) {
        let currentSelectedIndicator = viewWithTag(kIndicatorImageTag + selectedIndex) as! UIImageView
        currentSelectedIndicator.image = UIImage(named: "circle_stroke")
        selectedIndex = index
        let updateSelectedIndicator = viewWithTag(kIndicatorImageTag + selectedIndex) as! UIImageView
        updateSelectedIndicator.image = UIImage(named: "circle_fill")
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
