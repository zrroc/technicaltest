//
//  SurveyListViewModel.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift

class SurveyListViewModel {
    
    var surveyArray: Dynamic<[Survey]>
    var isSurveyDataLoadFromCache: Bool!
    
    init() {
        surveyArray = Dynamic()
    }
    
    /**
     Get the survey data form cache and online
     
     - parameter isNeedLoadFromCache: if true first load data from cache and continue to get online, if false skip load from cache
     */
    func getSurveysData(isNeedLoadFromCache:Bool) {
        if isNeedLoadFromCache {
            isSurveyDataLoadFromCache = true
            self.surveyArray.value = getCachedSurveysData()
        }
        HttpClient.sharedInstance.request(Router.GetSurveys()) { (data, error) in
            if let _ = error {
                self.surveyArray.value = []
            }else{
                var tempSurveyArray:[Survey] = []
                for surveyDic in (data as! [[String:AnyObject]]) {
                    if let survey = Mapper<Survey>().map(surveyDic) {
                        tempSurveyArray.append(survey)
                    }
                }
                self.isSurveyDataLoadFromCache = false
                self.surveyArray.value = tempSurveyArray
                self.cacheSurveysData(tempSurveyArray)
            }
        }
    }
    /**
     Cache survey data using Realm
     
     - parameter surveyArray: the survey model array need to store
     */
    func cacheSurveysData(surveyArray:[Survey]) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(realm.objects(Survey))
            realm.add(surveyArray)
        }
    }
    /**
     Get cached survey data form Realm
     
     - returns: the survey model array
     */
    func getCachedSurveysData() -> [Survey] {
        let realm = try! Realm()
        let surveys = realm.objects(Survey.self)
        return Array(surveys)
    }
}
