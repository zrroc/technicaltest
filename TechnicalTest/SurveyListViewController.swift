//
//  ViewController.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import UIKit

public let kSurveyListCellIdentifier = "surveyCell"
public let kIndicatorRightSpacing: CGFloat = 10
public let kActivityIndicatorWidth: CGFloat = 50

class SurveyListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,SurveyListViewCellDelegate,UIScrollViewDelegate {
    
    @IBOutlet weak var surveyListTableView: UITableView!
    var indicatorView: IndicatorView!
    var activityIndicator: UIActivityIndicatorView!
    
    /// bind the viewmodel data, once the data in viewmodel changed, the bind closure will execute
    var surveyListViewModel: SurveyListViewModel! {
        didSet{
            surveyListViewModel.surveyArray.bind { [unowned self] (value) in
                if !self.surveyListViewModel.isSurveyDataLoadFromCache {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.removeFromSuperview()
                    self.surveyListTableView.setContentOffset(CGPointZero, animated: false)
                    self.indicatorView.removeFromSuperview()
                }
                if value.count > 0 {
                    self.indicatorView = IndicatorView(numberOfIndicator: value.count)
                    self.indicatorView.frame.origin = CGPointMake(self.view.frame.width - self.indicatorView.frame.width - kIndicatorRightSpacing, (self.view.frame.height - self.indicatorView.frame.height)/2.0 + 64/2)
                    self.view.addSubview(self.indicatorView)
                }
                self.surveyListTableView.reloadData()
            }
        }
    }
    
    //MARK: LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator = UIActivityIndicatorView(frame: CGRectMake((view.frame.width - kActivityIndicatorWidth)/2.0, (view.frame.height - kActivityIndicatorWidth)/2.0 + 64/2, kActivityIndicatorWidth, kActivityIndicatorWidth))
        self.activityIndicator.color = UIColor(red:0.071,  green:0.114,  blue:0.204, alpha:1)
        
        surveyListViewModel = SurveyListViewModel()
        getSurveysData(true)
        
        surveyListTableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Method
    /**
     Get the survey data form cache and online
     
     - parameter loadFromCache: if true first load data from cache and continue to get online, if false skip load from cache
     */
    func getSurveysData(loadFromCache:Bool) {
        view.addSubview(self.activityIndicator)
        self.activityIndicator.startAnimating()
        surveyListViewModel.getSurveysData(loadFromCache)
    }
    
    //MARK: Action

    @IBAction func updateSurveysData(sender: AnyObject) {
        getSurveysData(false)
    }
    
    //MARK: UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let array = surveyListViewModel.surveyArray.value {
            return array.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(kSurveyListCellIdentifier) as! SurveyListViewCell
        if cell.delegate == nil {
            cell.delegate = self
        }
        cell.index = indexPath.row
        cell.configData(surveyListViewModel.surveyArray.value![indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return view.frame.height - 64
    }
    
    //MARK: SurveyListViewCellDelegate
    func cellDidTapedTakeSurveyButtonAtIndex(index: Int) {
        let vc = UIViewController()
        vc.view.backgroundColor = UIColor.whiteColor()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: UIScrollViewDelegate
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if indicatorView != nil {
            let index = Int(scrollView.contentOffset.y/(view.frame.height - 64))
            indicatorView.updateSelectedIndicatorIndex(index)
        }
    }
}

