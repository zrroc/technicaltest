//
//  Router.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import Foundation
import Alamofire

public let kAuthTokenKey = "access_token"

enum Router:URLRequestConvertible {
    static let baseUrlString = "https://www-staging.usay.co/app"
    static var oAuthToken: String {
        get {
            return "6eebeac3dd1dc9c97a06985b6480471211a777b39aa4d0e03747ce6acc4a3369"
        }
    }
    
    case GetSurveys()
    
    var method:Alamofire.Method {
        switch self {
        case .GetSurveys:
            return .GET
        }
    }
    
    var path:String {
        switch self {
        case .GetSurveys:
            return "/surveys.json"
        }
    }
    
    //MARK: URLRequestConvertible
    var URLRequest: NSMutableURLRequest {
        let url = NSURL(string: Router.baseUrlString)!
        let mutableURLRequest = NSMutableURLRequest(URL: url.URLByAppendingPathComponent(path))
        mutableURLRequest.HTTPMethod = method.rawValue
        
        switch self {
        case .GetSurveys:
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: [kAuthTokenKey:Router.oAuthToken]).0
        }
    }
    
    
}