//
//  SurveyListViewCell.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import UIKit
import Kingfisher

protocol SurveyListViewCellDelegate: class {
    /**
     The take survey button be tapped will invoke
     
     - parameter index: cell index in the tableview
     */
    func cellDidTapedTakeSurveyButtonAtIndex(index:Int)
}

class SurveyListViewCell: UITableViewCell {
    @IBOutlet weak var surveyBackgroundImageView: UIImageView!
    @IBOutlet weak var surveyNameLabel: UILabel!
    @IBOutlet weak var surveyDescriptionLabel: UILabel!
    
    weak var delegate: SurveyListViewCellDelegate?
    var index: Int! //cell index

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    /**
     config cell to show data
     
     - parameter survey: the survey model to show in cell
     */
    func configData(survey:Survey) {
        surveyBackgroundImageView.kf_setImageWithURL(NSURL(string: survey.surveyCoverImageUrl + "l")!)
        surveyNameLabel.text = survey.surveyTitle
        surveyDescriptionLabel.text = survey.surveyDescription
    }
    @IBAction func takeSurvey(sender: AnyObject) {
        delegate?.cellDidTapedTakeSurveyButtonAtIndex(index)
    }

}
