//
//  SurveyListViewControllerTests.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import XCTest
import ObjectMapper

@testable import TechnicalTest


class SurveyListViewControllerTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testShowData() {
        class MockSurveyListViewCell: SurveyListViewCell {
            let stubbedImageView: UIImageView = UIImageView()
            let stubbedNameLabel: UILabel = UILabel()
            let stubbedDescLabel: UILabel = UILabel()
            override var surveyBackgroundImageView: UIImageView! {
                get{
                    return stubbedImageView
                }
                set{
                    
                }
            }
            override var surveyNameLabel: UILabel! {
                get{
                    return stubbedNameLabel
                }
                set{
                    
                }
            }
            override var surveyDescriptionLabel: UILabel! {
                get{
                    return stubbedDescLabel
                }
                set{
                    
                }
            }
        }
        
        class MockSurveyListViewController: SurveyListViewController {
            let stubbedTableView: UITableView = UITableView(frame: CGRectZero)
            let stubbedActivityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
            let stubbedIndicatorView: IndicatorView = IndicatorView(numberOfIndicator: 1)
            override var surveyListTableView: UITableView! {
                get{
                    return stubbedTableView
                }
                set{
                    
                }
            }
            override var activityIndicator: UIActivityIndicatorView! {
                get{
                    return stubbedActivityIndicator
                }
                set{
                    
                }
            }
            override var indicatorView: IndicatorView! {
                get{
                    return stubbedIndicatorView
                }
                set{
                    
                }
            }
            private override func viewDidLoad() {
                surveyListTableView.dataSource = self
                surveyListTableView.registerClass(MockSurveyListViewCell.self, forCellReuseIdentifier: kSurveyListCellIdentifier)
                surveyListViewModel = SurveyListViewModel()
                getSurveysData(false)
            }
            override func getSurveysData(loadFromCache: Bool) {
                
                let mockData:[String:AnyObject] = ["cover_image_url":"https://dhdbhh0jsld0o.cloudfront.net/m/1773_","title":"testtitle","description":"testdescription"]
                surveyListViewModel.isSurveyDataLoadFromCache = loadFromCache
                surveyListViewModel.surveyArray.value = [Mapper<Survey>().map(mockData)!]
            }
        }

        
        let vc = MockSurveyListViewController()
        let _ = vc.view
        let cell :SurveyListViewCell = vc.surveyListTableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! SurveyListViewCell
        XCTAssertEqual(cell.surveyBackgroundImageView.kf_webURL?.URLString, vc.surveyListViewModel.surveyArray.value![0].surveyCoverImageUrl + "l")
        XCTAssertEqual(cell.surveyNameLabel.text, vc.surveyListViewModel.surveyArray.value![0].surveyTitle)
        XCTAssertEqual(cell.surveyDescriptionLabel.text, vc.surveyListViewModel.surveyArray.value![0].surveyDescription)
        XCTAssertNotNil(vc.indicatorView)
        XCTAssertTrue(vc.indicatorView.frame.height == ((kIndicatorWidth + kIndicatorSpacing) * CGFloat(vc.surveyListViewModel.surveyArray.value!.count) - kIndicatorSpacing))
    }
    
}
