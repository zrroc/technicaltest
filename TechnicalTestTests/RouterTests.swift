//
//  RouterTests.swift
//  TechnicalTest
//
//  Created by zrroc on 16/7/31.
//  Copyright © 2016年 Zrroc. All rights reserved.
//

import XCTest
@testable import TechnicalTest

class RouterTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetRequest() {
        let getSurveysUrl = "https://www-staging.usay.co/app/surveys.json?access_token=6eebeac3dd1dc9c97a06985b6480471211a777b39aa4d0e03747ce6acc4a3369"
        let getSurveyRequest = Router.GetSurveys()
        XCTAssertEqual(getSurveysUrl, getSurveyRequest.URLRequest.URLString)
    }
    
    
    
}
